/**
 * Created by Victor.Ikoro on 10/1/2016.
 */

var request = require("request");
var phantom = require('phantom');
var MongoClient = require('mongodb').MongoClient;

var REGISTERED_DRUGS_URL = "http://www.nafdac.gov.ng/index.php/product-registration/registered-drugs";
var REGISTERED_FOOD_PRODUCTS_URL = "http://www.nafdac.gov.ng/index.php/product-registration/registered-food-products";
var MEDICAL_PRODUCTS_URL = "http://www.nafdac.gov.ng/index.php/product-registration/medical-products";
var COSMETICS_PRODUCTS_URL = "http://www.nafdac.gov.ng/index.php/product-registration/listed-products-2";
var LISTED_PRODUCTS_URL = "http://www.nafdac.gov.ng/index.php/product-registration/listed-products";

//var MONGO_DB_URL= "mongodb://localhost:27017/nafdac";
var MONGO_DB_URL= "mongodb://ikorovictor:prometheus94@ds053156.mlab.com:53156/nafdac";

var params =
{
    drugs : REGISTERED_DRUGS_URL,
    food : REGISTERED_FOOD_PRODUCTS_URL,
    medicals : MEDICAL_PRODUCTS_URL,
    cosmetics : COSMETICS_PRODUCTS_URL,
    listed : LISTED_PRODUCTS_URL
}


var db = null;
var products_db_coll =  null;
var products=[];
var batchSize = 5000;
var totalCount = 0;

var start = function () {

    db.createCollection("products", function (err, products_collection) {
        if (!err) {
            products_db_coll =  products_collection;
            //products_collection.createIndex({regNumber : 1, name : 1}, {unique:true})
            var param = process.argv[process.argv.length - 1];
			if(params.hasOwnProperty(param))
            {
               loadPage(params[param], "POST", requestBody(param, batchSize, 0), param);
            }
            else{
                console.log("Unknown type : " + param);
                console.log("try =>  drugs|food|medicals|cosmetics|listed|  ..........................EXITING");
                process.exit(0);
            }
            
        }
    });
};

var evaluatePageData =  function()
{
    var data = "[]";
    if(Fabrik && Fabrik.blocks)
    {
        Object.keys(Fabrik.blocks).forEach(function(key)
        {
            var list = Fabrik.blocks[key];
            if(list.options && list.options.data && list.options.data.length > 0)
            {
                data =  JSON.stringify(list.options.data[0]);
            }
        });
    }
    return data;
};

var loadPage = function(url, method, formBody, param)
{
    var sitePage=null;
    var phInstance=null;
    phantom.create()
        .then(instance =>
        {
            phInstance = instance;
            return instance.createPage()
        })
        .then(page =>
        {
            sitePage = page;
            sitePage = setPageEventHandlers(sitePage);
            return sitePage.open(url, method,formBody);
        })
        .then(status => {
            console.log(status);
            return sitePage.property('content');
        })
        .then(content => {
            sitePage.evaluate(evaluatePageData).then(function(products, err)
            {
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    var product_array = JSON.parse(products);
					totalCount += product_array.length;
					insertProducts(param, product_array);
					console.log("TOTAL: " + totalCount);
				    if(product_array.length == batchSize)
					{
						console.log("GETTING NEXT BATCH");
						loadPage(url, "POST",  requestBody(param,batchSize, totalCount), param);
					}
                    else
                    {
                        console.log("FINISHED LOADING CONTENT............EXITING");
                        //process.exit(0);
                    }
					
                }
                sitePage.close();
                phInstance.exit();
            });
        }).catch(error =>
        {
            console.log(error);
            phInstance.exit();
        })

};

var setPageEventHandlers =  function(page)
{
    page.property("onResourceReceived",function(response)
    {
        page.evaluate(evaluatePageData);
    });
    return page;
};

var saveProduct = function(product)
{
    if(products_db_coll)
    {

        products_db_coll.updateOne({"regNumber": product.regNumber}, {$set: product}, {
            upsert: true, multiple : true
        }, function(err, r) {
                if(err)
                {
                    console.log(err);
                }
        });
    }
};

var insertProducts = function(param, products)
{
    if(products_db_coll)
    {
        products =  processProductsData(param, products);
        products_db_coll.insertMany(products, {w:1, keepGoing:true},  function(err, r) {
            if(err)
            {
                console.log(err);
            }
            else
            {
                console.log("products saved");
            }
        });
    }
};

var processProductsData = function(param, products)
{
        var processed = [];
        products.forEach(function(product)
        {
            var processedProduct = processProductData(param, product);
            processed.push(processedProduct);

        });
        return processed;

};


var requestBody = function(param, batchSize, batchStartIndex )
{
    switch(param)
    {
        case "food" :
            return "fabrik_list_filter_all_11_com_fabrik_11=&search-mode-advanced=all&limit11=" +  batchSize + "&limitstart11=" + batchStartIndex +"&option=com_fabrik&orderdir=&orderby=&view=list&listid=11&listref=11_com_fabrik_11&Itemid=834&fabrik_referrer=%2Findex.php%2Fproduct-registration%2Fregistered-food-products%3Fresetfilters%3D0%26limitstart11%3D10&2f796048e3592cb057287031d8b876af=1&format=html&_packageId=0&task=list.filter&fabrik_listplugin_name=&fabrik_listplugin_renderOrder=&fabrik_listplugin_options=&incfilters=1";
        case "drugs" :
            return "fabrik_list_filter_all_10_com_fabrik_10=&search-mode-advanced=all&limit10=" + batchSize + "&limitstart10=" + batchStartIndex + "&option=com_fabrik&orderdir=&orderby=&view=list&listid=10&listref=10_com_fabrik_10&Itemid=835&fabrik_referrer=&19b5024a81c89d42b28bbe323ea1e32f=1&format=html&_packageId=0&task=list.filter&fabrik_listplugin_name=&fabrik_listplugin_renderOrder=&fabrik_listplugin_options=&incfilters=1";
        case "medicals" :
            return "fabrik_list_filter_all_9_com_fabrik_9=&search-mode-advanced=all&limit9=" + batchSize + "&limitstart9=" +  batchStartIndex + "&option=com_fabrik&orderdir=&orderby=&view=list&listid=9&listref=9_com_fabrik_9&Itemid=836&fabrik_referrer=&ddabd32712ecafda7eab30b3ef6bfd16=1&format=html&_packageId=0&task=list.filter&fabrik_listplugin_name=&fabrik_listplugin_renderOrder=&fabrik_listplugin_options=&incfilters=1";
        case "cosmetics" :
            return "fabrik_list_filter_all_6_com_fabrik_6=&search-mode-advanced=all&limit6=" + batchSize + "&limitstart6=" + batchStartIndex + "&option=com_fabrik&orderdir=&orderby=&view=list&listid=6&listref=6_com_fabrik_6&Itemid=948&fabrik_referrer=&ddabd32712ecafda7eab30b3ef6bfd16=1&format=html&_packageId=0&task=list.filter&fabrik_listplugin_name=&fabrik_listplugin_renderOrder=&fabrik_listplugin_options=&incfilters=1";
        case "listed" :
            return "fabrik_list_filter_all_8_com_fabrik_8=&search-mode-advanced=all&limit8=" + batchSize + "&limitstart8=" +  batchStartIndex + "&option=com_fabrik&orderdir=&orderby=&view=list&listid=8&listref=8_com_fabrik_8&Itemid=833&fabrik_referrer=&ddabd32712ecafda7eab30b3ef6bfd16=1&format=html&_packageId=0&task=list.filter&fabrik_listplugin_name=&fabrik_listplugin_renderOrder=&fabrik_listplugin_options=&incfilters=1";
        default :
            console.log("Unknown request body index : " +  param);
            process.exit(0);
    }

};

var processProductData = function(param,rawProductData)
{
    var product = {};
    product.type = param.toLowerCase().trim();
    product.regNumber = rawProductData.data[param + "___RegistrationNo"].replace(/ /g, '');
    product.activeIngredient = rawProductData.data[param + "___ActiveIngredent"];
    product.manufacturer = rawProductData.data[param + "___Manufacturer"];
    product.name = rawProductData.data[param + "___ProductName"];
    product.group = rawProductData.data[param + "___ProductGroup"];
    product.rawData =  rawProductData.data;
    product.details = {};
    product.updatedOn = new Date();
    return product;
}

MongoClient.connect(MONGO_DB_URL, function (err, temp_db) {
    if (!err) {
        console.log("Connected correctly to db");
        db = temp_db;
        start();

    }
    else {
        console.log('DB ERROR: ' + err);
    }

});